package de.anton.rest.invoice.resources;

import de.anton.rest.invoice.constant.ErrorMessageCodes;
import de.anton.rest.invoice.db.entity.InvoiceEntity;
import de.anton.rest.invoice.db.repository.InvoiceRepositoryImpl;
import de.anton.rest.invoice.exception.InvoiceNotFoundException;
import de.anton.rest.invoice.exception.interceptor.InvoiceNotFoundExceptionInterceptor;
import de.anton.rest.invoice.model.response.ErrorMessageResponse;
import de.anton.rest.invoice.model.response.InvoiceResponse;
import de.anton.rest.invoice.service.InvoiceService;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.junit.Rule;
import org.junit.Test;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

public class InvoiceResourceTest {

    public static final Long ID = Long.valueOf(1);
    public static final String RECIPIENT = "recipient";
    public static final String DOCTOR = "doctor";
    public static final double AMOUNT = 12.20;
    public static final int INVOICE_ID_REQUEST = 1;
    private static final InvoiceRepositoryImpl INVOICE_REPOSITORY = mock(InvoiceRepositoryImpl.class);
    public static final String INVOICES_URI = "/invoices/";
    @Rule
    public final ResourceTestRule RESOURCES = ResourceTestRule.builder()
            .addResource(new InvoiceResource(new InvoiceService(INVOICE_REPOSITORY)))
            .addResource(new InvoiceNotFoundExceptionInterceptor())
            .build();

    @Test
    public void getInvoices_verify_findAll() {
        RESOURCES.client().target(INVOICES_URI)
                .request().get(new GenericType<List<InvoiceResponse>>() {
        });
        verify(INVOICE_REPOSITORY).findAll();
    }

    @Test(expected = NotFoundException.class)
    public void invalid_url_throws_404_exception() {
        RESOURCES.client().target("/wrong_url")
                .request().get(new GenericType<List<InvoiceResponse>>() {
        });
    }

    @Test
    public void findById_check_response() {
        InvoiceEntity invoiceEntity = new InvoiceEntity();
        invoiceEntity.setId(ID);
        invoiceEntity.setRecipient(RECIPIENT);
        invoiceEntity.setDoctor(DOCTOR);
        invoiceEntity.setAmount(AMOUNT);
        when(INVOICE_REPOSITORY.findById(anyLong())).thenReturn(Optional.of(invoiceEntity));
        InvoiceResponse invoiceResponse = RESOURCES.client()
                .target(INVOICES_URI + INVOICE_ID_REQUEST)
                .request().get(InvoiceResponse.class);
        assertThat(invoiceResponse.getId(), is(ID));
        assertThat(invoiceResponse.getRecipient(), is(RECIPIENT));
        assertThat(invoiceResponse.getDoctor(), is(DOCTOR));
        assertThat(invoiceResponse.getAmount(), is(AMOUNT));
    }

    @Test
    public void findById_check_custom_json_error_message_response() {
        when(INVOICE_REPOSITORY.findById(anyLong())).thenReturn(Optional.empty());
        Response response = RESOURCES.client()
                .target("/invoices/" + INVOICE_ID_REQUEST)
                .request().get();
        ErrorMessageResponse errorMessageResponse = response.readEntity(ErrorMessageResponse.class);
        assertThat(response.getStatus(), is(Response.Status.NOT_FOUND.getStatusCode()));
        assertThat(errorMessageResponse.getStatus(), is(Response.Status.NOT_FOUND.getStatusCode()));
        assertThat(errorMessageResponse.getMessage(), is(String.format(InvoiceService.INVOICE_WITH_ID_NOT_FOUND, INVOICE_ID_REQUEST)));
        assertThat(errorMessageResponse.getInvoiceServiceSpecificCode(), is(ErrorMessageCodes.getErrorMessageCode(InvoiceNotFoundException.class).get()));
    }


}