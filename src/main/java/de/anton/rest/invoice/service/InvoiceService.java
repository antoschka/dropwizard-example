package de.anton.rest.invoice.service;

import de.anton.rest.invoice.db.entity.InvoiceEntity;
import de.anton.rest.invoice.db.repository.InvoiceRepositoryImpl;
import de.anton.rest.invoice.db.repository.InvoiceRepositoryInterface;
import de.anton.rest.invoice.exception.InvoiceNotFoundException;
import de.anton.rest.invoice.model.converter.Invoice2InvoiceEntityConverter;
import de.anton.rest.invoice.model.request.InvoiceRequest;
import de.anton.rest.invoice.model.response.InvoiceResponse;

import java.util.List;
import java.util.stream.Collectors;

public class InvoiceService {

    public static final String INVOICE_WITH_ID_NOT_FOUND = "Invoice with id '%d' not found";
    private static final Invoice2InvoiceEntityConverter INVOICE_CONVERTER = new Invoice2InvoiceEntityConverter();
    InvoiceRepositoryInterface invoiceRepository;

    public InvoiceService(InvoiceRepositoryImpl invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }


    public List<InvoiceResponse> findAll() {
        return invoiceRepository.findAll().stream()
                .map(invoiceEntity -> INVOICE_CONVERTER.to(invoiceEntity))
                .collect(Collectors.toList());
    }

    public InvoiceResponse findById(final long id) throws InvoiceNotFoundException {
        return INVOICE_CONVERTER.to(invoiceRepository.findById(id).orElseThrow(() ->
                new InvoiceNotFoundException(String.format("Invoice with id '%d' not found", id))));
    }

    public InvoiceResponse save(final InvoiceRequest invoiceRequest) {
        return INVOICE_CONVERTER.to(invoiceRepository.save(INVOICE_CONVERTER.from(invoiceRequest)));
    }

    public InvoiceResponse update(final long id, InvoiceRequest invoiceRequest) throws InvoiceNotFoundException {
        InvoiceEntity invoiceEntity = invoiceRepository.findById(id).orElseThrow(() ->
                new InvoiceNotFoundException(String.format(INVOICE_WITH_ID_NOT_FOUND, id)));
        return INVOICE_CONVERTER.to(invoiceRepository.update(INVOICE_CONVERTER.updateInvoiceEntity(invoiceRequest, invoiceEntity)));
    }

    public void deleteById(long id) throws InvoiceNotFoundException {
        InvoiceEntity invoiceEntity = invoiceRepository.findById(id).orElseThrow(() ->
                new InvoiceNotFoundException(String.format(INVOICE_WITH_ID_NOT_FOUND, id)));
        invoiceRepository.delete(invoiceEntity);
    }
}
