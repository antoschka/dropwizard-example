package de.anton.rest.invoice.resources;

import com.codahale.metrics.annotation.Timed;
import de.anton.rest.invoice.exception.InvoiceNotFoundException;
import de.anton.rest.invoice.model.request.InvoiceRequest;
import de.anton.rest.invoice.model.response.InvoiceResponse;
import de.anton.rest.invoice.service.InvoiceService;
import io.dropwizard.hibernate.UnitOfWork;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/invoices")
@Produces(MediaType.APPLICATION_JSON)
public class InvoiceResource {

    private InvoiceService invoiceService;

    public InvoiceResource(final InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @GET()
    @Timed
    @UnitOfWork
    public List<InvoiceResponse> getInvoices() {
        return invoiceService.findAll();
    }

    @POST()
    @Consumes({MediaType.APPLICATION_JSON})
    @Timed
    @UnitOfWork
    public InvoiceResponse createInvoices(@NotNull @Valid InvoiceRequest invoice) {
        return invoiceService.save(invoice);
    }

    @GET()
    @Path("{id}")
    @Timed
    @UnitOfWork
    public InvoiceResponse getInvoiceById(@PathParam("id") long id) throws InvoiceNotFoundException {
        return invoiceService.findById(id);
    }

    @PUT()
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Timed
    @UnitOfWork
    public InvoiceResponse updateInvoiceById(@PathParam("id") long id, @NotNull @Valid InvoiceRequest invoice) throws InvoiceNotFoundException {
        return invoiceService.update(id, invoice);
    }

    @DELETE()
    @Path("{id}")
    @Timed
    @UnitOfWork
    public void deleteInvoiceById(@PathParam("id") long id) throws InvoiceNotFoundException {
        invoiceService.deleteById(id);
    }
}
