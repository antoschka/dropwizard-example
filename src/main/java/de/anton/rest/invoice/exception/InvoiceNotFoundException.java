package de.anton.rest.invoice.exception;


import javax.ws.rs.ext.Provider;

@Provider
public class InvoiceNotFoundException extends Exception {

    public InvoiceNotFoundException(String msg) {
        super(msg);
    }

}
