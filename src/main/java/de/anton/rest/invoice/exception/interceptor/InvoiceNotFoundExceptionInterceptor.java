package de.anton.rest.invoice.exception.interceptor;

import de.anton.rest.invoice.constant.ErrorMessageCodes;
import de.anton.rest.invoice.exception.InvoiceNotFoundException;
import de.anton.rest.invoice.model.response.ErrorMessageResponse;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class InvoiceNotFoundExceptionInterceptor implements
        ExceptionMapper<InvoiceNotFoundException> {

    @Override
    public Response toResponse(InvoiceNotFoundException e) {
        return Response.status(Response.Status.NOT_FOUND).entity(
                new ErrorMessageResponse(Response.Status.NOT_FOUND.getStatusCode(),
                        ErrorMessageCodes.getErrorMessageCode(InvoiceNotFoundException.class).get(),
                        e.getMessage()))
                .type(MediaType.APPLICATION_JSON).build();
    }
}
