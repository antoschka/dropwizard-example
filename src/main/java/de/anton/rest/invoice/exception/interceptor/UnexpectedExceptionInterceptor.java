package de.anton.rest.invoice.exception.interceptor;

import io.dropwizard.jersey.errors.ErrorMessage;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import java.io.PrintWriter;
import java.io.StringWriter;

// Generischer Interceptor, der aber nicht registriert ist. Nur als Beispiel gedacht.
public class UnexpectedExceptionInterceptor implements ExceptionMapper<Throwable> {

    @Override
    public Response toResponse(Throwable throwable) {
        StringWriter errorStackTrace = new StringWriter();
        throwable.printStackTrace(new PrintWriter(errorStackTrace));
        ErrorMessage errorMessage = new ErrorMessage(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), throwable.getMessage(), errorStackTrace.toString());
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(errorMessage)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}
