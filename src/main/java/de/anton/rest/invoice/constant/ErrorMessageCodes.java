package de.anton.rest.invoice.constant;

import de.anton.rest.invoice.exception.InvoiceNotFoundException;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.Optional.of;

public class ErrorMessageCodes {

    public static final Integer INVOICE_NOT_FOUND = 101;

    private static final Map<Class<? extends Exception>, Optional<Integer>> classToExceptionMessageCodes =
            new ConcurrentHashMap<Class<? extends Exception>, Optional<Integer>>() {{
                put(InvoiceNotFoundException.class, of(INVOICE_NOT_FOUND));
            }};

    private ErrorMessageCodes() {
    }

    public static Optional<Integer> getErrorMessageCode(Class<? extends Exception> exceptionClass) {
        return classToExceptionMessageCodes.getOrDefault(exceptionClass, Optional.of(0));
    }
}
