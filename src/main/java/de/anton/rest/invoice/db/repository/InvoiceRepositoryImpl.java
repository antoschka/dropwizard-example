package de.anton.rest.invoice.db.repository;

import de.anton.rest.invoice.db.entity.InvoiceEntity;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;
import java.util.Optional;

public class InvoiceRepositoryImpl extends AbstractDAO<InvoiceEntity> implements InvoiceRepositoryInterface {

    public InvoiceRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<InvoiceEntity> findAll() {
        return list(namedQuery("de.anton.rest.invoice.db.entity.InvoiceEntity.findAll"));

    }

    @Override
    public Optional<InvoiceEntity> findById(long id) {
        return Optional.ofNullable(get(id));
    }

    @Override
    public InvoiceEntity save(InvoiceEntity invoiceEntity) {
        return persist(invoiceEntity);
    }

    @Override
    public InvoiceEntity update(InvoiceEntity invoiceEntity) {
        return persist(invoiceEntity);
    }

    @Override
    public void delete(InvoiceEntity invoiceEntity) {
        currentSession().delete(invoiceEntity);
    }
}
