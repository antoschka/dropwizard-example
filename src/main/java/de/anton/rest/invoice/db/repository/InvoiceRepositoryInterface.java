package de.anton.rest.invoice.db.repository;

import de.anton.rest.invoice.db.entity.InvoiceEntity;

import java.util.List;
import java.util.Optional;

public interface InvoiceRepositoryInterface {

    List<InvoiceEntity> findAll();

    Optional<InvoiceEntity> findById(final long id);

    InvoiceEntity save(final InvoiceEntity invoiceEntity);

    InvoiceEntity update(final InvoiceEntity invoiceEntity);

    void delete(final InvoiceEntity invoiceEntity);
}
