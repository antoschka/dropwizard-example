package de.anton.rest.invoice.db.entity;

import javax.persistence.*;

@Entity
@Table(name = "document")
public class DocumentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "page")
    private Integer page;
    @Column(name = "recipient")
    private String recipient;
    @ManyToOne
    @JoinColumn(name = "invoice_id_fk")
    private InvoiceEntity invoiceEntity;

    public DocumentEntity() {
    }

    public DocumentEntity(final int page, final String recipient, InvoiceEntity invoiceEntity) {
        this.page = page;
        this.recipient = recipient;
        this.invoiceEntity = invoiceEntity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public InvoiceEntity getInvoiceEntity() {
        return invoiceEntity;
    }

    public void setInvoiceEntity(InvoiceEntity invoiceEntity) {
        this.invoiceEntity = invoiceEntity;
    }
}
