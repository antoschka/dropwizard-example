package de.anton.rest.invoice.db.repository;

import de.anton.rest.invoice.db.entity.DocumentEntity;
import de.anton.rest.invoice.db.entity.InvoiceEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DummyInvoiceRepositoryImpl implements InvoiceRepositoryInterface {

    @Override
    public List<InvoiceEntity> findAll() {
        ArrayList<InvoiceEntity> invoiceEntities = new ArrayList<>();
        InvoiceEntity dummyInvoice = getDummyInvoice();
        InvoiceEntity dummyInvoice2 = getDummyInvoice();
        dummyInvoice2.setDocuments(new ArrayList<>());
        invoiceEntities.add(dummyInvoice);
        invoiceEntities.add(dummyInvoice2);
        return invoiceEntities;
    }

    @Override
    public Optional<InvoiceEntity> findById(long id) {
        return Optional.of(getDummyInvoice());
    }

    private InvoiceEntity getDummyInvoice() {
        InvoiceEntity invoiceEntity = new InvoiceEntity();
        invoiceEntity.setId(Long.valueOf(1));
        invoiceEntity.setDoctor("doctor");
        invoiceEntity.setRecipient("recipient");
        invoiceEntity.setAmount(12.12);
        ArrayList<DocumentEntity> documents = new ArrayList<>();
        DocumentEntity documentFirst = new DocumentEntity(1, "docRecipient", invoiceEntity);
        documentFirst.setId(1l);
        DocumentEntity documentSecond = new DocumentEntity(1, "docRecipient2", invoiceEntity);
        documentSecond.setId(2l);
        documents.add(documentFirst);
        documents.add(documentSecond);
        invoiceEntity.setDocuments(documents);
        return invoiceEntity;
    }

    @Override
    public InvoiceEntity save(InvoiceEntity invoiceEntity) {
        return getDummyInvoice();
    }

    @Override
    public InvoiceEntity update(InvoiceEntity invoiceEntity) {
        return getDummyInvoice();
    }

    @Override
    public void delete(InvoiceEntity invoiceEntity) {
    }
}
