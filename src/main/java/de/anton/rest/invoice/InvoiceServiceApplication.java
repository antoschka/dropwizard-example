package de.anton.rest.invoice;

import de.anton.rest.invoice.db.entity.DocumentEntity;
import de.anton.rest.invoice.db.entity.InvoiceEntity;
import de.anton.rest.invoice.db.repository.InvoiceRepositoryImpl;
import de.anton.rest.invoice.exception.interceptor.InvoiceNotFoundExceptionInterceptor;
import de.anton.rest.invoice.resources.InvoiceResource;
import de.anton.rest.invoice.service.InvoiceService;
import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class InvoiceServiceApplication extends Application<InvoiceServiceConfiguration> {

    private final HibernateBundle<InvoiceServiceConfiguration> hibernate = new HibernateBundle<InvoiceServiceConfiguration>(InvoiceEntity.class, DocumentEntity.class) {
        @Override
        public DataSourceFactory getDataSourceFactory(InvoiceServiceConfiguration configuration) {
            return configuration.getDataSourceFactory();
        }
    };

    public static void main(final String[] args) throws Exception {
        new InvoiceServiceApplication().run(args);
    }

    @Override
    public String getName() {
        return "InvoiceService";
    }

    @Override
    public void initialize(final Bootstrap<InvoiceServiceConfiguration> bootstrap) {
        super.initialize(bootstrap);
        bootstrap.addBundle(hibernate);
    }

    @Override
    public void run(final InvoiceServiceConfiguration configuration,
                    final Environment environment) {
        final InvoiceRepositoryImpl invoiceRepository
                = new InvoiceRepositoryImpl(hibernate.getSessionFactory());
        final InvoiceResource resource = new InvoiceResource(new InvoiceService(invoiceRepository));
        environment.jersey().register(resource);
        environment.jersey().register(new InvoiceNotFoundExceptionInterceptor());
        // Nur als Beispiel, falls ein generischer Interceptor gebraucht wird.
//        environment.jersey().register(new UnexpectedExceptionInterceptor());
    }

}
