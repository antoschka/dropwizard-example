package de.anton.rest.invoice.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.anton.rest.invoice.model.Document;

import java.util.List;

public class InvoiceResponse {

    private long id;

    private String recipient;

    private String doctor;

    private double amount;

    private List<Document> documents;

    public InvoiceResponse() {
    }

    public InvoiceResponse(long id, String recipient, String doctor, double amount, List<Document> documents) {
        this.id = id;
        this.recipient = recipient;
        this.doctor = doctor;
        this.amount = amount;
        this.documents = documents;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @JsonProperty
    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    @JsonProperty
    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    @JsonProperty
    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @JsonProperty
    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }
}
