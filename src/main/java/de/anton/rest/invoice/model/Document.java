package de.anton.rest.invoice.model;

import com.fasterxml.jackson.annotation.JsonProperty;

// Eine Klasse für die Requests und Responses (alternativer Ansatz). Die Id wird im Converter nicht gemapped, selbst wenn diese mitgegeben wird.
public class Document {

    private long id;
    private int page;
    private String recipient;

    public Document() {
    }

    public Document(long id, int page, String recipient) {
        this.id = id;
        this.page = page;
        this.recipient = recipient;
    }

    @JsonProperty
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @JsonProperty
    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    @JsonProperty
    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }
}
