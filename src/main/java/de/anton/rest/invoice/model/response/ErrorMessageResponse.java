package de.anton.rest.invoice.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorMessageResponse {

    int status;
    int invoiceServiceSpecificCode;
    String message;

    public ErrorMessageResponse() {
    }

    public ErrorMessageResponse(int status, int invoiceServiceSpecificCode, String message) {
        this.status = status;
        this.invoiceServiceSpecificCode = invoiceServiceSpecificCode;
        this.message = message;
    }

    @JsonProperty
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @JsonProperty
    public int getInvoiceServiceSpecificCode() {
        return invoiceServiceSpecificCode;
    }

    public void setInvoiceServiceSpecificCode(int invoiceServiceSpecificCode) {
        this.invoiceServiceSpecificCode = invoiceServiceSpecificCode;
    }

    @JsonProperty
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
