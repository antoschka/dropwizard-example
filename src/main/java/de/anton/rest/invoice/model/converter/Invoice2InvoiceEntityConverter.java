package de.anton.rest.invoice.model.converter;

import de.anton.rest.invoice.db.entity.DocumentEntity;
import de.anton.rest.invoice.db.entity.InvoiceEntity;
import de.anton.rest.invoice.model.Document;
import de.anton.rest.invoice.model.request.InvoiceRequest;
import de.anton.rest.invoice.model.response.InvoiceResponse;

import java.util.stream.Collectors;

public class Invoice2InvoiceEntityConverter {


    public InvoiceResponse to(final InvoiceEntity invoiceEntity) {
        InvoiceResponse invoiceResponse = new InvoiceResponse(invoiceEntity.getId(),
                invoiceEntity.getRecipient(),
                invoiceEntity.getDoctor(),
                invoiceEntity.getAmount(),
                invoiceEntity.getDocuments().stream()
                        .map(documentEntity -> new Document(documentEntity.getId(), documentEntity.getPage(), documentEntity.getRecipient()))
                        .collect(Collectors.toList()));
        return invoiceResponse;
    }

    public InvoiceEntity from(final InvoiceRequest invoiceRequest) {
        final InvoiceEntity invoiceEntity = new InvoiceEntity();
        invoiceEntity.setDoctor(invoiceRequest.getDoctor());
        invoiceEntity.setRecipient(invoiceRequest.getRecipient());
        invoiceEntity.setAmount(invoiceRequest.getAmount());
        invoiceRequest.getDocuments().stream().forEach(document -> invoiceEntity.getDocuments()
                .add(new DocumentEntity(document.getPage(), document.getRecipient(), invoiceEntity)));
        return invoiceEntity;
    }

    public InvoiceEntity updateInvoiceEntity(final InvoiceRequest invoiceRequest, final InvoiceEntity invoiceEntity) {
        invoiceEntity.setDoctor(invoiceRequest.getDoctor());
        invoiceEntity.setRecipient(invoiceRequest.getRecipient());
        invoiceEntity.setAmount(invoiceRequest.getAmount());
        invoiceEntity.getDocuments().clear();
        invoiceRequest.getDocuments().stream().forEach(document -> invoiceEntity.getDocuments()
                .add(new DocumentEntity(document.getPage(), document.getRecipient(), invoiceEntity)));
        return invoiceEntity;
    }
}
