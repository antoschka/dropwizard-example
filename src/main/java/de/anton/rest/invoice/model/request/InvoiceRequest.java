package de.anton.rest.invoice.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.anton.rest.invoice.model.Document;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.List;

public class InvoiceRequest {

    @NotEmpty
    private String recipient;
    @NotEmpty
    private String doctor;
    @NotNull
    private Double amount;
    private List<Document> documents;

    public InvoiceRequest() {
    }

    @JsonProperty
    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    @JsonProperty
    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    @JsonProperty
    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @JsonProperty
    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }
}
